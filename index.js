(function() {
  var colors = {
    default: '\033[0m',
    gray: '\033[90m',
    cyan: '\033[36m',
    red: '\033[31m',
    green: '\033[32m',
    blue: '\033[34m',
    yellow: '\033[33m'
  };

  function set (text, color) {
    return colors[color] + text + colors.default;
  }

  function key(key, value) {
    return '  ' + set(key + ' : ', 'gray') + set(value, 'green');
  }

  exports.set = set;
  exports.key = key;

}).call(this);

